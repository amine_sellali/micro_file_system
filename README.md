**Réalisation d'un micro système de fichiers**

**Objectifs**

Il vous est demandé de :

réaliser un micro système de fichiers ;
le système de fichiers doit résider dans un fichier de 8 Mo ;
le système de fichiers est géré par un exécutable obtenu à partir d'un programme C ;
L'éxécutable prend deux arguments, le premier est le chemin du fichier dans lequel réside le système de fichiers, les paramètres suivants concernent l'action à appliquer sur le système de fichiers ;
le micro système de fichier ne comporte qu'un répertoire : le répertoire principal, le répertoire principal peut comporter au maximum 64 fichiers, un fichier est caractérisé par un nom de 16 caractères au maximum et ses blocs de données, un fichier peut comporter au maximum 2040 blocs de données ;
un bloc de données fait 256 octets et les blocs sont numérotés sur 2 octets ;
les différentes actions possibles sur le système de fichiers sont :

- TYPE pour créer un fichier si possible, le nom du fichier suit la commande, le contenu du fichier est donné en entrée standard de l'exécutable ;
- CAT pour afficher un fichier, le nom du fichier suit la commande ;
- RM pour détruire un fichier, le nom du fichier suit la commande ;
- MV pour renommer un fichier, les noms original et nouveau du fichier suivent la commande ;
- CP pour copier un fichier, les noms de l'original et de la copie du fichier suivent la commande ;

**Matériel nécessaire**

Un PC sous Linux.

**Création du système de fichiers :**

dd if=/dev/zero of=filesystem.bin bs=256 count=32768

**Compilation :**

gcc picofs.c -o picofs

**Exécution de LS, TYPE, CAT, RM, MV ou CP :**

- ./picofs filesystem.bin LS
- ./picofs filesystem.bin TYPE mon_fichier.txt
- ./picofs filesystem.bin CAT mon_fichier.txt
- ./picofs filesystem.bin RM mon_fichier.txt
- ./picofs filesystem.bin MV mon_fichier.txt mon_fichier2.txt
- ./picofs filesystem.bin CP mon_fichier.txt mon_fichier2.txt


